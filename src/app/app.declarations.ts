
import { AppComponent } from './root/app.component';
import * as COMPONENTS from './components/index';
import * as PAGES from './pages/index';
import { PricePipe } from './providers/PricePipe ';

export const DECLARATIONS: any[] = [
    AppComponent,
    PricePipe,
    COMPONENTS.HeaderComponent,
    COMPONENTS.FooterComponent,
    COMPONENTS.DemanderDevisComponent,
    COMPONENTS.AssuranceAutomobileComponent,
    COMPONENTS.SlideShowComponent,
    COMPONENTS.LoginComponent,
    COMPONENTS.DemanderDevisEnligneComponent,
    COMPONENTS.DriverInfoComponent,
    COMPONENTS.CarInfoComponent,
    COMPONENTS.GarantiesComponent,
    COMPONENTS.PaymentComponent,
    COMPONENTS.RecapitulatifComponent,
    COMPONENTS.ManagerComponent,
    COMPONENTS.ConditionsSpecialComponent,
    COMPONENTS.CongratComponent,
    COMPONENTS.ConventionComponent,
    COMPONENTS.GererUsersComponent,
    COMPONENTS.ConventionParamComponent,
    COMPONENTS.GererConventionComponent,
    COMPONENTS.AddUserModalComponent,
    COMPONENTS.EditUserModalComponent,
    COMPONENTS.AddConventionModalComponent,
    COMPONENTS.EditConventionModalComponent,
    COMPONENTS.AddConvParamModalComponent,
    COMPONENTS.EditConvParamModalComponent,
    COMPONENTS.EditPasswordModalComponent,
    COMPONENTS.DeleteConfirmComponent,
    COMPONENTS.HeaderLoginComponent,
    COMPONENTS.TierceComponent,
    COMPONENTS.CollisionComponent,
    COMPONENTS.PersonneTransporteComponent,
    COMPONENTS.JustificatifComponent,
    PAGES.DemanderDevisPageComponent,
    PAGES.LoginPageComponent,
    PAGES.MainPageComponent,
    PAGES.ManagementComponent,
    PAGES.HeaderNavComponent,
    PAGES.AdminManagementComponent
];
