import { ApiProvider } from 'src/app/providers/api.provider';
import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { MatDialog } from '@angular/material';
import { EditPasswordModalComponent } from '../edit-password-modal/edit-password-modal.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input('user') user;
  userConnectedId: any;
  userRoleConnected: string;

  constructor(
    private auth: AuthService,
    private dialog: MatDialog,
  ) {
    this.userConnectedId = localStorage.getItem('Iduserconnect');
    this.userRoleConnected = localStorage.getItem('userRole');
     }


  ngOnInit() {
      this.userConnectedId = localStorage.getItem('Iduserconnect');
      this.userRoleConnected = localStorage.getItem('userRole');

  }

  editPassword(){
    const dialogRef = this.dialog.open(EditPasswordModalComponent,  {
      height: '',
      width: '40%',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  logout() {
    this.auth.logout();
  }
}
