import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-login',
  templateUrl: './header-login.component.html',
  styleUrls: ['./header-login.component.css']
})
export class HeaderLoginComponent implements OnInit {

  constructor(
    private readonly router: Router
  ) {   
   }

  ngOnInit() {
  }

  goToAdminSpace(){
    this.router.navigate(['manager'])
  }
}
