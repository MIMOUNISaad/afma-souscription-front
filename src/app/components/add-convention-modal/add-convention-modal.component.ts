import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiProvider } from 'src/app/providers/api.provider';
import { Toaster } from 'src/app/providers/toast/toast';

const valeurPattern = '^(([0-9]+)|([0-9]+[\.][0-9]{1,2}))$';
@Component({
  selector: 'app-add-convention-modal',
  templateUrl: './add-convention-modal.component.html',
  styleUrls: ['./add-convention-modal.component.css']
})
export class AddConventionModalComponent implements OnInit {

  agences: any;
  form: FormGroup;
  submitted = false;
  
  configuration: any[] = [
                        {code : "P", titre : "Prime"},
                        {code : "T", titre : "Taux"}];
  reponseAdd: any;
  garantiesInfo: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<AddConventionModalComponent>,
              private api: ApiProvider,
              private fb: FormBuilder,
              private toast: Toaster) { }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

  ngOnInit() {
    this.initForm();
    this.getGaranties();
  }
  initForm(): void {
    this.form = this.fb.group({
      code: [null, Validators.required],
      configuration: [null, Validators.required],
      taux: [null, [Validators.required, Validators.pattern(valeurPattern)]],
      prime: [null, [Validators.required, Validators.pattern(valeurPattern)]],
      franchise: [null, [Validators.required, Validators.pattern(valeurPattern)]],
    });
  }
  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return (this.submitted && this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }
  validForm() {
    if(this.f.code.value === 'RC' || this.f.code.value === 'DR')
    {return this.f.code.valid;}
    else
    {
      return this.f.code.valid && this.f.configuration.valid && this.f.franchise.valid &&
    ((this.f.configuration.value === 'T' && this.f.taux.valid) ||
    (this.f.configuration.value === 'P' && this.f.prime.valid));
  }

  }
  getGaranties(){
    let that = this;
    this.api.getInfoGaranties().subscribe(
      res => {
        that.garantiesInfo = res;
      },
      err => {
        that.toast.showToastmessageBack(err.error.message)
      }
    )
  }
  addConvention(){ 
    if(this.validForm()){
    let that = this;
    this.api.addConvention(this.form.value, this.data).subscribe(
    res => {
      that.reponseAdd = res;
      that.toast.showSuccessMessage("Ajoutée avec succée! ");
      that.closeDialog(this.dialogRef);
    },
    err => {
      that.toast.showToastmessageBack(err.error.message)
    }
  )
  }
     this.submitted = true; 
   
  }

  getAgences() {
    let that = this;
    this.api.getAgences().subscribe(
      res => { 
        that.agences = res;
      },
      err => {

      }
    );
  }
}
