import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddConventionModalComponent } from './add-convention-modal.component';

describe('AddConventionModalComponent', () => {
  let component: AddConventionModalComponent;
  let fixture: ComponentFixture<AddConventionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddConventionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddConventionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
