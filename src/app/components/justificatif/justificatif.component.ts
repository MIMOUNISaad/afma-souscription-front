import { Component, Input,Output,EventEmitter,SimpleChanges, OnChanges, OnInit } from '@angular/core';
import { ApiProvider } from 'src/app/providers/api.provider';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Toaster } from 'src/app/providers/toast/toast';


@Component({
  selector: 'app-justificatif',
  templateUrl: './justificatif.component.html',
  styleUrls: ['./justificatif.component.css']
})
export class JustificatifComponent  implements OnChanges{
  @Input('toggle') toggle: boolean;
  @Output() onNext = new EventEmitter<any>();
  
  files:any[] =[];
  form: FormGroup;
  submitted = false;
  
  constructor(
    private api: ApiProvider,
    private fb: FormBuilder,
    private toast: Toaster
  ) {
    this.initForm();
  }


  initForm(): void {
    this.form = this.fb.group({
      cin: [null, [Validators.required]],
      cg: [null, [Validators.required]],
      pc: [null, [Validators.required]],
     
    });
  }
  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return (this.submitted && this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }
  cinUpload(fileInput){
    let file = fileInput.target.files[0];
    let splitted = file.name.split('.',2);
    if(splitted[1] !=='pdf' && splitted[1] !=='jpg' && splitted[1] !=='PNG' ){
      this.toast.showToastmessageBack("Format du fichier invalide ! ");
            fileInput.target.value = ''; 
      return;
    }
    this.files.push(file);
  }

  cgUpload(fileInput){
    let file = fileInput.target.files[0];
    let splitted = file.name.split('.',2);
    if(splitted[1] !=='pdf' && splitted[1] !=='jpg' && splitted[1] !=='PNG' ){
      this.toast.showToastmessageBack("Format du fichier invalide ! ");
            fileInput.target.value = ''; 
      return;
    }
   
    this.files.push(file);
  }
  pcUpload(fileInput){
    let file = fileInput.target.files[0];
    let splitted = file.name.split('.',2);
    if(splitted[1] !=='pdf' && splitted[1] !=='jpg' && splitted[1] !=='PNG' ){
      this.toast.showToastmessageBack("Format du fichier invalide ! ");
            fileInput.target.value = ''; 
      return;
    }
    this.files.push(file);
  }

  validForm(){
    return this.f.cin.valid && this.f.cg.valid && this.f.pc.valid;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['toggle'] && !changes['toggle'].firstChange) {
      const data = {
        valid: this.validForm(),
        value: {justificatifsData: this.files}
      };
      this.onNext.emit(data);
      }
    }
  }

