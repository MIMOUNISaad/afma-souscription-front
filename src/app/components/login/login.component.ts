import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { ApiProvider } from 'src/app/providers/api.provider';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  submitted: boolean;
  myuserconnected: string;
  roleConnected: string;
  error: any;
  loginDTO:any = {};

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private readonly api: ApiProvider,
    private router: Router
  ) {  
    
    this.myuserconnected = localStorage.getItem('Iduserconnect');
    this.roleConnected = localStorage.getItem('userRole');
    if(this.myuserconnected !== null) {
        if(this.roleConnected === 'admin') {
            this.router.navigate(['/admin-management/users']);
            } 
        else if(this.roleConnected === 'user')
          {
            this.router.navigate(['/management']);
          }
    }
  }

  ngOnInit() {
    this.form = this.fb.group({
      captcha:[null,Validators.required],
      code: [null, Validators.required],
      cin: [null, Validators.required]
    });
    let that = this;                     // a utiliser pour l'affichage de l'user
    this.auth.isErrorLogin.subscribe(
        res => {
          that.error = res;
        }
      );
  }

  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.submitted)
    );
  }


  onSubmit() {
    if (this.form.valid) {
    this.loginDTO.login = this.form.get('cin').value;
    this.loginDTO.password = this.form.get('code').value;
    this.loginDTO.captcha = this.form.get('captcha').value;
    
    this.auth.login(this.loginDTO);
    }
    this.submitted = true;
  }

}


