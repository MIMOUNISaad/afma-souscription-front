import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-slide-show',
  templateUrl: './slide-show.component.html',
  styleUrls: ['./slide-show.component.css']
})
export class SlideShowComponent{

  constructor(
    private readonly router: Router
  ) { }

  navigate(): void {
    this.router.navigate(['/demander-devis']);
  }
}
