import { ConditionsSpecialComponent } from './../conditions-special/conditions-special.component';
import { Component, OnInit, OnChanges, Injectable } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import {MatDialog} from '@angular/material';
import { ApiProvider } from './../../providers/api.provider';
import { Toaster } from 'src/app/providers/toast/toast';

@Injectable()
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnChanges {

  @Input('donnees') donnees: any
  
  @Input('toggle') toggle: boolean;
  @Input('total') total: any;
  conditionModal:boolean = false ;
  @Output() onNext = new EventEmitter<any>();
  @Output() payment  = new EventEmitter<any>();

  lesfractions: any = ['1', '2', '3', '4', '6', '8', '12'];
  form: FormGroup;
  submitted = false;
  captcha = new FormControl(false);
  paymentType = 'online';
  ville: any;

  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private api: ApiProvider,
    private toast: Toaster
  ) {
    let that = this;
    this.api.getAgences().subscribe(
      res => {
       
        that.agences = res;
        that.initForm();
      },
      err => {
        this.toast.showToast();
      }
    )
  }

  _agences: any[] = [
    {
      city: "Casablanca",
      address: "22 Bd Moulay Youssef CASABLANCA",
      phone: ["05 22 42 80 00"]
    }, {
      city: "Rabat",
      address: "Rue RIAD Residence RIAD A 1er étage Appartement 1 HASSAN RABAT",
      phone: ["05 37 73 50 55", "05 37 70 26 77"]
    }, {
      city: "Agadir",
      address: "Immeuble ATLASSIA 4 Avenue Mohamed V AGADIR",
      phone: ["05 28 82 55 09"]
    }, {
      city: "Marrakech",
      address: "Bd abdelkrim el khattabi Borj MENARA 2 APT 8 2éme etage MARRAKECH",
      phone: ["05 24 43 80 00"]
    }, {
      city: "Fes",
      address: "1 rue Ghssane kanafani bureau tayba apt 3 1er etage FES",
      phone: ["05 35 94 49 99"]
    }, {
      city: "Tanger",
      address: "Résidence Yamna Angle Bd Mohammed V et Youssef Bnou Tachfine TANGER",
      phone: ["05 39 94 36 35"]
    }, {
      city: "Oujda",
      address: "6 BD MOHAMED V Immeuble El yamama 1er étage apt n° 4 OUJDA",
      phone: ["05 36 71 16 07", "05 36 71 16 08"]
    },
    {
      city: "Laâyoune",
      address: "AVENUE MED 6, RUE ASSILA APPT 1 N ° 182, LAAYOUNE",
      phone: ["05 28 99 03 90", "05 28 99 07 14"]
    },
  ];

  agences;

  initForm(): void { 
    this.form = this.fb.group({
      ville: [null, Validators.required],
      paymentType: [this.paymentType, Validators.required],
      fraction: [1, Validators.required],
      accepter: [false, Validators.requiredTrue],
      total: [this.total],
      city: [null]
    });
  }

  get calculateFractions() {
      return this.total / parseInt(this.form.get('fraction').value);
  }

  openDialog() {
    const dialogRef = this.dialog.open(ConditionsSpecialComponent);

    dialogRef.afterClosed().subscribe(result => {
    });
  }


  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return this.submitted && this.form.controls[attr].invalid;
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['toggle'] && !changes['toggle'].firstChange) {
      this.submitted = true;
      if(this.form.valid) {
        let city = this.agences.filter(ag => {
          return ag.agenceId == this.form.get('ville').value;
        })[0];
        this.form.get('city').setValue(city);
      }
      this.onNext.emit({
        valid: this.form.valid && this.captcha.value ,
        value: this.form.value
      });
    }
  }

  onScriptLoad() {
  }

  changePaymentType1(){
    this.payment.emit('online');
  }
  changePaymentType2(){
    this.payment.emit('cheque');
  }

  onScriptError() {
      console.log('Something went long when loading the Google reCAPTCHA')
  }

}
