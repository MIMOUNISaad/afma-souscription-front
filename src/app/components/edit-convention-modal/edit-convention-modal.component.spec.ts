import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditConventionModalComponent } from './edit-convention-modal.component';

describe('EditConventionModalComponent', () => {
  let component: EditConventionModalComponent;
  let fixture: ComponentFixture<EditConventionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditConventionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditConventionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
