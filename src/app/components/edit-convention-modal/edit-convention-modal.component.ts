import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiProvider } from 'src/app/providers/api.provider';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Toaster } from 'src/app/providers/toast/toast';

const valeurPattern = '^(([0-9]+)|([0-9]+[\.][0-9]{1,2}))$';
@Component({
  selector: 'app-edit-convention-modal',
  templateUrl: './edit-convention-modal.component.html',
  styleUrls: ['./edit-convention-modal.component.css']
})
export class EditConventionModalComponent implements OnInit {
  agences: any;
  form: FormGroup;
  ville: any;
  submitted = false;
  conventions: any;
  partener: any;
  garantiesInfo: any;

  configuration: any[] = ['T', 'P'];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<EditConventionModalComponent>,
              private api: ApiProvider,
              private fb: FormBuilder,
              private toast: Toaster) {
              
               }

              
  closeDialog(value) {
    this.dialogRef.close(value);
  }

  ngOnInit() {
    this.getGaranties();
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      code: [this.data[1].code],
      configuration: [this.data[1].configuration, Validators.required],
      taux: [this.data[1].taux, [Validators.required, Validators.pattern(valeurPattern)]],
      prime: [this.data[1].prime, [Validators.required, Validators.pattern(valeurPattern)]],
      franchise: [this.data[1].franchise, [Validators.required, Validators.pattern(valeurPattern)]],
    });
  }
  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return (this.submitted && this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }

  validForm() {
    return this.f.code.valid && this.f.configuration.valid && this.f.franchise.valid &&
    ((this.f.configuration.value === 'T' && this.f.taux.valid) ||
    (this.f.configuration.value === 'P' && this.f.prime.valid));
  }

  getGaranties(){
    let that = this;
    this.api.getInfoGaranties().subscribe(
      res => {
        that.garantiesInfo = res;
      },
      err => {
        that.toast.showToastmessageBack(err.error.message)
      }
    )
  }
  editConvention() {
    if(this.f.configuration.value === 'T')
      this.form.get('prime').setValue(null);
    else
    this.form.get('taux').setValue(null);
    
    let that = this;
    //if (this.validForm()) {
    this.api.editConvention(this.form.value, this.data[0], this.data[1].conventionId).subscribe(
      res => { 
              that.conventions = res; 
              that.toast.showSuccessMessage("la modification bien effectuée ");
              that.closeDialog(this.dialogRef);
      },
      err => {
        that.toast.showToastmessageBack(err.error);
      }
    );
    // }
    // this.submitted = true;
  }
}
