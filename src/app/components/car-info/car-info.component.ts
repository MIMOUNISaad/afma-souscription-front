import { ApiProvider } from './../../providers/api.provider';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, Injectable } from '@angular/core';
import { Toaster } from 'src/app/providers/toast/toast';

const valeurPattern = '^(([0-9]+)|([0-9]+[\.][0-9]{1,2}))$';
const codeImmaPattern = '^[1-9][0-9]{0,4}$';
const letterImmaPattern = '^[A-Za-z]$';
const villeImmaPattern = '^[1-9][0-9]?$';
const wwImmaPattern = '^[1-9][0-9]{0,6}$';
const datePattern = "^(((0[1-9]|[12][0-9]|3[01])/(0[13578]|1[02])/((19|[2-9][0-9])[0-9]{2}))|((0[1-9]|[12][0-9]|30)/(0[13456789]|1[012])/((19|[2-9][0-9])[0-9]{2}))|((0[1-9]|1[0-9]|2[0-8])/02/((19|[2-9][0-9])[0-9]{2}))|(29/02/((1[6-9]|[2-9][0-9])(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$";

@Injectable()
@Component({
  selector: 'app-car-info',
  templateUrl: './car-info.component.html',
  styleUrls: ['./car-info.component.css']
})
export class CarInfoComponent implements OnChanges {

  @Input('toggle') toggle: boolean;
  @Output() onNext = new EventEmitter<any>();
  form: FormGroup;
  submitted = false;
  currentDate = new Date();

  usageOptions = [{'libelle': 'Tourisme', 'code': 'A'}];
  puissanceOptions = ['6', '7', '8', '9', '10', '11', '12', '13', '14', '15'];
  carburantOptions = [{
    label: 'Diesel',
    value: 'D'
  }, {
    label: 'Essence',
    value: 'E'
  }];
  neuf = null;
  venale = null;
  invalidVenale = true;
  typeImmatriculation = 'definitif';

  dateDebutCouverture = null;

  constructor(
    private fb: FormBuilder,
    private api: ApiProvider,
    private toast: Toaster
    ) {
    this.initForm();
    this.subcribeToFormChanges();
    //this.getCrm();
  }

 /* getCrm(): void {
    let that = this;
    this.api.getCrm().subscribe(
      res => {
        that.form.get('crm').setValue(res['crm']);
      },
      err => {
        this.toast.showToast();
      }
    );
  }
*/
  initForm(): void {
    this.form = this.fb.group({
      marque: [null, Validators.required],
      usage: [null, Validators.required],
      carburant: [null, Validators.required],
      puissance: [null, Validators.required],
      dateMEC: [null, [Validators.required, this.validDate, this.vcalidateDateMEC]],
      neuf: [null, [Validators.required, Validators.pattern(valeurPattern)]],
      venale: [null, [Validators.required, Validators.pattern(valeurPattern)]],
      glace: [null, [Validators.required, Validators.pattern(valeurPattern)]],
      debutCouvertue: [null, [Validators.required, this.validDate, this.validDateDebutCouverture]],
      finCouverture: [null, [Validators.required, this.validDate, this.validDateFinCouverture]],
      //crm: [10, Validators.required],
      typeImma: [this.typeImmatriculation, Validators.required],
      immatriculationDefinitf: this.fb.group({
        code: [null, [Validators.required, Validators.pattern(codeImmaPattern)]],
        letter: [null, [Validators.required, Validators.pattern(letterImmaPattern)]],
        ville: [null, [Validators.required, Validators.pattern(villeImmaPattern)]]
      }),
      immatriculationWw: [null, [Validators.required, Validators.pattern(wwImmaPattern)]],
      _dateMEC: [null],
      _debutCouvertue: [null],
      _finCouverture: [null]
    });
  }

  subcribeToFormChanges(): void {
    const that = this;
    const myFormValueChanges$ = that.form.valueChanges;
    myFormValueChanges$.subscribe(function(event) {
      if (that.neuf !== event.neuf || that.venale !== event.venale) {
        that.neuf = event.neuf;
        that.venale = event.venale;
        that.invalidVenale = parseInt(event.neuf) < parseInt(event.venale);
      }
      if (that.typeImmatriculation !== event.typeImma) {
        that.typeImmatriculation = event.typeImma;
        that.form.get('debutCouvertue').setValue(that.form.get('debutCouvertue').value);
      }
      if (that.dateDebutCouverture !== event.debutCouvertue) {
        that.dateDebutCouverture = event.debutCouvertue;
        that.form.get('finCouverture').setValue(that.form.get('finCouverture').value);
      }
    });
  }

  getVenaleValue(e: any) {
    if ( this.f.puissance.valid && this.f.dateMEC.valid && this.f.neuf.valid) {
      let date = this.f.dateMEC.value.slice(0, 2) + '/' + this.f.dateMEC.value.slice(2);
      date = date.slice(0, 5) + '/' + date.slice(5);
      const that = this;
      this.api.getValeurVenale(this.f.puissance.value, this.f.neuf.value, date).subscribe(
        res => {
          that.form.get('venale').setValue(res.toString().replace('.', ','));
        },
        err => {
         // that.toast.showToast();
        }
      );
    } else {
      this.form.get('venale').setValue(null);
    }
  }


  validDate(c: FormControl) {
    if(c.value && c.value.length === 8) {
      let date = c.value.slice(0, 2) + '/' + c.value.slice(2);
      date = date.slice(0, 5) + '/' + date.slice(5);
      return RegExp(datePattern).test(date) ? null : {
        ValidateDate: true
      };
    } else {
      return {
        ValidateDate: true
      };
    }
  }

  vcalidateDateMEC(c: FormControl) {
    if(c.value && c.value.length === 8) {
      let date = c.value.slice(0, 2) + '/' + c.value.slice(2);
      date = date.slice(0, 5) + '/' + date.slice(5);

      if(RegExp(datePattern).test(date)) {
        let dateArray = date.split('/');
        let d = new Date(parseInt(dateArray[2]), parseInt(dateArray[1]) - 1, parseInt(dateArray[0])).setHours(0,0,0,0);
        let today = new Date().setHours(0,0,0,0);
        if (d < today) {
          c.root.get('_dateMEC').setValue(d);
          return null;
        } else {
          return {
            ValidateDate: true
          };
        }
      } else {
        return {
          ValidateDate: true
        };
      }
    } else {
      return {
        ValidateDate: true
      };
    }
  }

  validDateFinCouverture(c: FormControl) {
    if(c.value && c.value.length === 8) {
      let date = c.value.slice(0, 2) + '/' + c.value.slice(2);
      date = date.slice(0, 5) + '/' + date.slice(5);
      let dateArray = date.split('/');
      let d = new Date(parseInt(dateArray[2]), parseInt(dateArray[1]) - 1, parseInt(dateArray[0])).setHours(0,0,0,0);
      if(d && c.root.get('debutCouvertue') && c.root.get('debutCouvertue').valid) {
        let debut = c.root.get('debutCouvertue').value.slice(0, 2) + '/' + c.root.get('debutCouvertue').value.slice(2);
        debut = debut.slice(0, 5) + '/' + debut.slice(5);
        let _dateArray = debut.split('/');
        let _debut = new Date(parseInt(_dateArray[2]), parseInt(_dateArray[1]) - 1, parseInt(_dateArray[0])).setHours(0,0,0,0);
        let yearLater = _debut + ( 1000 * 60 * 60 * 24 * 365 );
        if ( d > _debut && d <= yearLater ) {
            c.root.get('_finCouverture').setValue(d);
            return null;
        } else {
          return {
            ValidateDateFinCouverture: true
          };
        }
      } else {
        return {
          ValidateDateFinCouverture: true
        };
      }
    } else {
      return {
        ValidateDateFinCouverture: true
      };
    }
  }

  validDateDebutCouverture(c: FormControl) {
    if(c.value && c.value.length === 8) {
      let date = c.value.slice(0, 2) + '/' + c.value.slice(2);
      date = date.slice(0, 5) + '/' + date.slice(5);
      let dateArray = date.split('/');
      let d = new Date(parseInt(dateArray[2]), parseInt(dateArray[1]) - 1, parseInt(dateArray[0])).setHours(0,0,0,0);
      if(d) {
        let type = c.root.get('typeImma') ? c.root.get('typeImma').value : null;
        let today = new Date().setHours(0,0,0,0);
        if(type === 'definitif') {
          if(d >= today + 86400000) {
            c.root.get('_debutCouvertue').setValue(d);
            return null;
          } else {
            return {
              ValidateDateDebutCouverture: true
            };
          }
        } else if(type === 'ww') {
          if(d >= today) {
            c.root.get('_debutCouvertue').setValue(d);
            return null;
          } else {
            return {
              ValidateDateDebutCouverture: true
            };
          }
        } else {
          return {
            ValidateDateDebutCouverture: true
          };
        }
      } else {
        return {
          ValidateDateDebutCouverture: true
        };
      }
    } else {
      return {
        ValidateDateDebutCouverture: true
      };
    }
  }

  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return this.submitted && (this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }

  validForm() {
    return this.f.marque.valid && this.f.usage.valid && this.f.carburant.valid && this.f.puissance.valid && this.f.dateMEC.valid && this.f.neuf.valid && this.f.venale.valid && this.f.glace.valid && this.f.debutCouvertue.valid && this.f.finCouverture.valid && this.f.typeImma.valid && ((this.f.typeImma.value === 'definitif' && this.f.immatriculationDefinitf.valid) || (this.f.typeImma.value === 'ww' && this.f.immatriculationWw.valid));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['toggle'] && !changes['toggle'].firstChange) {
      this.submitted = true;
      if(this.form.get('dateMEC').valid) {
        let date = this.form.get('dateMEC').value.slice(0, 2) + '/' + this.form.get('dateMEC').value.slice(2);
        date = date.slice(0, 5) + '/' + date.slice(5);
        let dateArray = date.split('/');
        let d = new Date(parseInt(dateArray[2]), parseInt(dateArray[1]) - 1, parseInt(dateArray[0])).setHours(0,0,0,0);
        this.form.get('_dateMEC').setValue(d);
      }
      this.onNext.emit({
        valid: this.validForm() && !this.invalidVenale,
        value: this.form.value
      });
    }
  }

}
