import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemanderDevisEnligneComponent } from './demander-devis-enligne.component';

describe('DemanderDevisEnligneComponent', () => {
  let component: DemanderDevisEnligneComponent;
  let fixture: ComponentFixture<DemanderDevisEnligneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemanderDevisEnligneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemanderDevisEnligneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
