import { Toaster } from './../../providers/toast/toast';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ApiProvider } from 'src/app/providers/api.provider';


const nomPattern = "^[a-z A-Z]+$";
const emailPattern = '^[\.a-zA-Z0-9_-]+@[a-zA-Z0-9-]+[\.]{1}[\.a-zA-Z0-9-]+$';
@Component({
  selector: 'app-add-user-modal',
  templateUrl: './add-user-modal.component.html',
  styleUrls: ['./add-user-modal.component.css']
})
export class AddUserModalComponent implements OnInit {
  agences: any;
  form: FormGroup;

  submitted = false;
  users: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<AddUserModalComponent>,
              private api: ApiProvider,
              private fb: FormBuilder,
              private toast: Toaster) { }

  ngOnInit() {
    this.initForm();
    this.getAgences();
  }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

  initForm(): void {
    this.form = this.fb.group({
      matricule: [null, Validators.required],
      nom: [null, [Validators.required, Validators.pattern(nomPattern)]],
      prenom: [null, [Validators.required, Validators.pattern(nomPattern)]],
      login: [null, [Validators.required, Validators.pattern(emailPattern)]],
      agence: [this.data, Validators.required],
      password: [null, Validators.required]
    });
  }
  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return (this.submitted && this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }
 
  showUsersByagences(){
    this.api.getUsersByAgences(this.f.agence.value).subscribe(
      res => {
        this.users = res;
      },
      err => {
        this.toast.showToastmessageBack("Erreur lors de la récupération des Utilisateurs !")
            }
    )
  }
  Ajouteruser(){
    let that = this;
    if(this.form.valid){
    this.api.addUser(this.form.value, this.f.agence.value).subscribe(
      res => { 
        that.users = res;
        that.closeDialog(this.dialogRef);
        that.toast.showSuccessMessage("Ajouter avec succée");  
      },
      err => {
        that.toast.showToastmessageBack(err.error.message);
      }
    );
    }
    this.submitted =true;
  }

  getAgences() {
    let that = this;
    this.api.getAgences().subscribe(
      res => {
        that.agences = res;
      },
      err => {

      }
    );
  }

}
