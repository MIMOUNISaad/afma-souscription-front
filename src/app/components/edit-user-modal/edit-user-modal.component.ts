import { Toaster } from './../../providers/toast/toast';
import { ApiProvider } from './../../providers/api.provider';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

const nomPattern = "^[a-z A-Z]+$";
const emailPattern = '^[\.a-zA-Z0-9_-]+@[a-zA-Z0-9-]+[\.]{1}[\.a-zA-Z0-9-]+$';
@Component({
  selector: 'app-edit-user-modal',
  templateUrl: './edit-user-modal.component.html',
  styleUrls: ['./edit-user-modal.component.css']
})
export class EditUserModalComponent implements OnInit {
  agences: any;  
  submitted = false;
  users: Object;
  ville: any;
  cityToUpdate: any;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<EditUserModalComponent>,
              private fb: FormBuilder,
              private api: ApiProvider,
              private  toast: Toaster
  )  {  }

  form: FormGroup;
  closeDialog(value) {
    this.dialogRef.close(value);
  }


  ngOnInit() {
    this.getAgences();
    this.initForm();
    //this.f.agence.setValue(this.data.agenceId);
  }


  initForm(): void {
    this.form = this.fb.group({
      matricule: [this.data.matricule, Validators.required],
      nom: [this.data.nom, [Validators.required, Validators.pattern(nomPattern)]],
      prenom: [this.data.prenom, [Validators.required, Validators.pattern(nomPattern)]],
      login: [this.data.login, [Validators.required, Validators.pattern(emailPattern)]],
      agence: [this.data.agenceId, Validators.required],
      utilisateurId: [this.data.utilisateurId]
    });
  }
  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return (this.submitted && this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }
  getAgences() {
    let that = this;
    this.api.getAgences().subscribe(
      res => {
        that.agences = res;
    /*    that.agences.forEach((ag, index) => {
          if(ag.agenceId === that.f.agence.value) {  
            that.cityToUpdate = ag.ville;
          }
          
        })
    */
      },
      err => {

      }
    );
  }

  modifierUser(){ 
    let that = this;
    if(this.form.valid) {
    this.api.editUser(this.form.value).subscribe(
      res => { 
              that.users = res; 
              that.toast.showSuccessMessage("la modification bien effectuée ");
              that.closeDialog(this.dialogRef);
      },
      err => {
        that.toast.showToastmessageBack(err.error.message);
      }
    );
    }
    this.submitted = true;
    
  }
}
