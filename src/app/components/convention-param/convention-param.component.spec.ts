import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConventionParamComponent } from './convention-param.component';

describe('ConventionParamComponent', () => {
  let component: ConventionParamComponent;
  let fixture: ComponentFixture<ConventionParamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConventionParamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConventionParamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
