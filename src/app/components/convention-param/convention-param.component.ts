import { Toaster } from './../../providers/toast/toast';
import { AddConvParamModalComponent } from './../add-conv-param-modal/add-conv-param-modal.component';
import { Component, OnInit } from '@angular/core';
import { ApiProvider } from 'src/app/providers/api.provider';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { EditConvParamModalComponent } from '../edit-conv-param-modal/edit-conv-param-modal.component';
import { DeleteConfirmComponent } from '../delete-confirm/delete-confirm.component';


@Component({
  selector: 'app-convention-param',
  templateUrl: './convention-param.component.html',
  styleUrls: ['./convention-param.component.css']
})
export class ConventionParamComponent implements OnInit {

  convParams: any;
  form: FormGroup;
  parteners: any;
  convParam: any;
  taille: any;

  constructor(
    public dialog: MatDialog,
    private readonly api: ApiProvider,
    private router: Router,
    private fb: FormBuilder,
    private toast: Toaster
  ) {
  }

  ngOnInit() {
    this.api.myuserconnected = localStorage.getItem('Iduserconnect');
    let myuserconnected = localStorage.getItem('Iduserconnect');

    if (myuserconnected === null) {
      this.router.navigate(['/manager']);
    } else {
      this.getPartener();
      //this.showConventionsParam();
    }
  }
  initForm(): void {
    this.form = this.fb.group({
      partener: [this.parteners[0].partenaireId]
    });
    this.showConventionsParam();
  }
  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return this.form.controls[attr].invalid;
  }
  getPartener() {
    let that = this;
    this.api.getPartener().subscribe(
      res => {
        that.parteners = res;
        that.initForm();
      },
      err => {
        that.toast.showToastmessageBack(err.error.message);
      }
    );
  }

  showConventionsParam() {

    let that = this;
    this.api.getConventionParam(this.f.partener.value).subscribe(
      res => {
        that.convParam = res;
        that.taille = this.convParam.length;
      },
      err => {
        that.toast.showToastmessageBack("Erreur lors de la récupération des conventions Param !");
      }
    );
  }

  AddConvParamFunction() {
    const dialogRef = this.dialog.open(AddConvParamModalComponent, {
      height: '',
      width: '70%',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.showConventionsParam();
    });
  }
  deleteConventionParam(id) {

    const dialogRef = this.dialog.open(DeleteConfirmComponent, {
      width: '350px',
      data: "Supprimer?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let that = this;
        this.api.deleteConvParam(id).subscribe(
          res => {
            //that.reponse = res;
            that.toast.showSuccessMessage("Suppression effectuée  ");
            this.showConventionsParam();
          },
          err => {
            that.toast.showToastmessageBack("erreur de suppression ! ");

          }
        );
      }
    });
  }

  EditConvParamFunction(convParams) {
    const that = this;
    const dialogRef = this.dialog.open(EditConvParamModalComponent, {
      data: [convParams, this.f.partener.value],
      height: '',
      width: '70%'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.showConventionsParam();

    });

  }
}
