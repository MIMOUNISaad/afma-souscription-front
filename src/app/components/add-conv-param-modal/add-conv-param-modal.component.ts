import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { ApiProvider } from 'src/app/providers/api.provider';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Toaster } from 'src/app/providers/toast/toast';

const valeurPattern = '^(([0-9]+)|([0-9]+[\.][0-9]{1,2}))$';
const placepattern = '^([0-9]+)$';

@Component({
  selector: 'app-add-conv-param-modal',
  templateUrl: './add-conv-param-modal.component.html',
  styleUrls: ['./add-conv-param-modal.component.css']
})
export class AddConvParamModalComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  partenerNoConv: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<AddConvParamModalComponent>,
              private api: ApiProvider,
              private fb: FormBuilder,
              private toast: Toaster) { }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

 
  ngOnInit() {
    this.initForm();
    this.getPartenerNoConvParam();
  }


  initForm(): void {
    this.form = this.fb.group({
      place: [null, [Validators.required, Validators.pattern(placepattern)]],
      franchise: [null, [Validators.required, Validators.pattern(valeurPattern)]],
      libelle: [null, Validators.required],
      invalidite: [null, [Validators.required, Validators.pattern(valeurPattern)]],
      frais: [null, [Validators.required, Validators.pattern(valeurPattern)]],
      deces: [null, [Validators.required, Validators.pattern(valeurPattern)]],
      cnpac: [null, [Validators.required, Validators.pattern(valeurPattern)]],
      accesoires: [null, [Validators.required, Validators.pattern(valeurPattern)]],
      partener: [null, Validators.required]
    });
  }
  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return (this.submitted && this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }

  getPartenerNoConvParam() { 
      let that = this;
      this.api.getPartenaireNotConvParam().subscribe(
        res => {
          that.partenerNoConv = res;         },
        err => {
          that.toast.showToastmessageBack(err.error.message)
        }
      )
    }
  addConventionParam(){ 
    if(this.form.valid)
    { 
      let that = this;
      this.api.addConventionParam(this.form.value,  this.f.partener.value).subscribe(
        res => {
          //that.reponseAdd = res;
          that.toast.showSuccessMessage("Ajouté");
          that.closeDialog(this.dialogRef);
        },
        err => {
          
          that.toast.showToastmessageBack("erreur !")
        }
      )
    }
    this.submitted = true;
  }


}
