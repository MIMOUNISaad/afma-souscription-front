import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddConvParamModalComponent } from './add-conv-param-modal.component';

describe('AddConvParamModalComponent', () => {
  let component: AddConvParamModalComponent;
  let fixture: ComponentFixture<AddConvParamModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddConvParamModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddConvParamModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
