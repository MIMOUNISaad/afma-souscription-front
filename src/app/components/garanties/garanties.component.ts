import { Component, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import {CollisionComponent, PersonneTransporteComponent, TierceComponent} from '..';
import {MatDialog} from '@angular/material';
import { ApiProvider } from './../../providers/api.provider';

@Component({
  selector: 'app-garanties',
  templateUrl: './garanties.component.html',
  styleUrls: ['./garanties.component.css']
})
export class GarantiesComponent implements OnChanges {

  @Input('toggle') _toggle: boolean;
  @Input('_garanties') _garanties: any;
  @Input('withCovention') _withConvention : boolean;
  @Output() onNext = new EventEmitter<any>();

  garanties: any[] = [{
   code: 'RC',
   lebelle: 'Responsabilité civile',
   selected: true,
   desc:"Cette garantie, obligatoire, consiste en la prise en charge des dommages causés à un tiers lorsque votre responsabilité est engagée à l'occasion d'un accident impliquant votre véhicule.",
   icon: '1',
   option: 0
 },{
  code:'DR',
  lebelle:'Défense et recours',
  selected:true,
  desc: 'Couvre dans la limite du capital assuré, les frais, de procédure tendant à :Pouvoir à la défense des intérêts de l’assuré et de toute personne autorisée par lui à utiliser et conduire le véhicule assuré, en cas de poursuites pénales fondées sur la circulation et l’utilisation dudit véhicule. Obtenir la réparation des dommages.',
  icon:"2",
  option: 0
},{
  code:"I",
  lebelle:'Incendie',
  selected:false,
  desc:"Couvre les dommages occasionnés au véhicule par un incendie combustion spontanée, chute de la foudre, explosion, dans les conditions et les limites contractuellement prévues.",
  icon:'3',
  option: 0
},{
  code:"V",
  lebelle:'Vol',
  selected:false,
  desc:"Couvre les dommages résultants de la disparistion totale du véhicule et les détériorations qu'il peut avoir subies à l'occasion d'une tentative de vol expréssement prévues dans les dispositions contractuelles.",
  icon:"4",
  option: 0
},{
  code:"T",
  lebelle:'Tierce',
  selected:false,
  desc:"Cette garantie prend en charge les dégâts subis par votre véhicule lors d'un accident dont la résponsabilité vous incombe (après déduction d'une franchise variant de 3% à 20%). Elle est conseillée pour les véhicules moins de trois ans.",
  icon:'5',
  option: 0
},{
  code:"TRC",
  lebelle:'Collisions',
  selected:false,
  desc:"Comme pour la tierce, cette garantie consiste en la prise en charge des dégâts subis par votre véhicule, mais en cas d'accident avec un tiers identifié.",
  icon:"6",
  option: 0
},{
  code:"BG",
  lebelle:'Bris de glace',
  selected:false,
  desc:"Couvre les dommages causés ou non par un accident subi par les glaces latérale, pare-brise ou lunette arrière du véhicule assuré, dans les conditions et limites contractuellement prévues.",
  icon:"7",
  option: 0
}
,{
  code:"A",
  lebelle:'Assistance',
  selected:false,
  desc:"L’assistance  au véhicule a pour but la mise en place et la prise en charge du remorquage d’un véhicule et le transport de ses occupants dans le cas d’un accident ou d’une panne.",
  icon:'8',
  option: 0
},{
  code:"PP",
  lebelle:'Personnes transportées',
  selected:false,
  desc:"Assurance contre les accidents corporels pouvant atteindre le conducteur, les personnes transportées au bord du véhicule, et couvrant le décès, l'incapacité permanente, ainsi que le remoboursement des frais médicaux dans les condistions et limites contractuellement prévues.",
  icon:"18",
  option: 0
},{
  code:"PF",
  lebelle:'Perte financière',
  selected:false,
  desc:"",
  icon:"12",
  option: 0
},
{
  code: 'IN',
  lebelle: 'Inondation',
  selected: false,
  desc:"Couvre les dommages matériels directs subis par le véhicule assuré et ayant pour cause l'action de l'eau provoquée par le ruissellement, l'engorgement ou le refoulement des égouts.",
  icon: '10',
  option: 0
},
{
  code: 'VR',
  lebelle: 'Vol isolé des rétroviseurs',
  selected: false,
  desc: '',
  icon: '',
  option: 0,
  myClass: 'my-garanty'
},
{
  code: 'VP',
  lebelle: 'Vol des pneumatiques',
  selected: false,
  desc: '',
  icon: '',
  option: 0,
  myClass: 'my-garanty'
},
{
  code: 'BMR',
  lebelle: 'Bris du mirroir des rétroviseurs et des optiques',
  selected: false,
  desc: '',
  icon: '',
  option: 0,
  myClass: 'my-garanty'
}];
  sommeGaranties = 0;
  tierceIndex = null;
  collisionIndex = null;

  constructor(public dialog: MatDialog,
    private api: ApiProvider) {}

  toggle(index, code){
    if(index === 0) return;
    if(index === this.tierceIndex && this._garanties[this.collisionIndex] && this._garanties[this.collisionIndex].selected) return;
    if(index === this.collisionIndex && this._garanties[this.tierceIndex] && this._garanties[this.tierceIndex].selected) return;
    let garantie = this._garanties[index];
    garantie.selected ? this.sommeGaranties -= garantie.prime : this.sommeGaranties += garantie.prime;
    garantie.selected = !garantie.selected;

    if (code === 'PP' && garantie.selected && this.api.withConvention === true) {
      this.showPPModal(index);
    } else if (code === 'T' && garantie.selected && this.api.withConvention === true) {
      this.showTModal(index);
    } else if (code === 'TRC' && garantie.selected && this.api.withConvention === true) {
      this.showTRCModal(index);
    }
  }

  retrieveClass(index){
    let garantie = this._garanties[index];
    if(garantie.selected == true){
      return "garantie selected";
    }else{
      return "garantie";
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    let that = this;
    if (changes['_toggle'] && !changes['_toggle'].firstChange) {
      const data = {
        valid: true,
        value: {
          garantiesData: this._garanties.filter(gar => { return gar.selected; }),
          sommeGaranties: that.sommeGaranties}
      };
      this.onNext.emit(data);
    }
    if(changes['_garanties'] && !changes['_garanties'].firstChange) {
      if(this._garanties && this._garanties.length) {
        this._garanties.map((gar, index) => {
          let _gar = that.garanties.filter(item => {
            return item.code === gar.code;
          })[0];
          if(gar.code === 'T') that.tierceIndex = index;
          if(gar.code === 'TRC') that.collisionIndex = index;
          gar.lebelle = _gar.lebelle;
          gar.selected = _gar.selected;
          gar.desc = _gar.desc;
          gar.icon = _gar.icon;
          gar.option = _gar.option;
          gar.myClass = _gar.myClass;
        });
        this.sommeGaranties = this._garanties[0].prime;
      }
    }
  }

  showPPModal(index) {
    const dialogRef = this.dialog.open(PersonneTransporteComponent, {
      data: 'PP',
      height: '240px',
      width: '390px',
      disableClose: true
    });
    let self = this;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        self._garanties[index].option = result;
      }
    });
  }
 
  showTModal(index) {
    const dialogRef = this.dialog.open(TierceComponent, {
      data: 'T',
      height: '240px',
      width: '390px',
      disableClose: true
    });
    let self = this;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        self._garanties[index].option = result;
      }
    });
  }

  showTRCModal(index) {
    const dialogRef = this.dialog.open(CollisionComponent, {
      data: 'TRC',
      height: '240px',
      width: '390px',
      disableClose: true
    });
    let self = this;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        self._garanties[index].option = result;
      }
    });
  }
}
