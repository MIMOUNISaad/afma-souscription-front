import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-tierce',
  templateUrl: './tierce.component.html',
  styleUrls: ['./tierce.component.css']
})
export class TierceComponent {

  form: FormGroup;
  submitted = false;

  tierceOptions = [{
    label: '3% des dommages avec min 1500 DH',
    value: 1
  }, {
    label: '5% des dommages avec min 2500 DH',
    value: 2
  }, {
    label: '10% des dommages avec min 3500 DH',
    value: 3
  }];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<TierceComponent>,
    private fb: FormBuilder,
  ) {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      code: [this.data],
      option: [1, Validators.required]
    });
  }

  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return this.submitted && (this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }

  validForm() {
    return this.f.option.valid;
  }

  closeDialog(value) {
    this.dialogRef.close(parseInt(value));
  }
}
