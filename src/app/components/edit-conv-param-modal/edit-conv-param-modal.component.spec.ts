import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditConvParamModalComponent } from './edit-conv-param-modal.component';

describe('EditConvParamModalComponent', () => {
  let component: EditConvParamModalComponent;
  let fixture: ComponentFixture<EditConvParamModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditConvParamModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditConvParamModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
