import { Component, OnInit, Inject } from '@angular/core';
import { ApiProvider } from 'src/app/providers/api.provider';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Toaster } from 'src/app/providers/toast/toast';

const valeurPattern = '^(([0-9]+)|([0-9]+[\.][0-9]{1,2}))$';
const placepattern = '^([0-9]+)$';

@Component({
  selector: 'app-edit-conv-param-modal',
  templateUrl: './edit-conv-param-modal.component.html',
  styleUrls: ['./edit-conv-param-modal.component.css']
})
export class EditConvParamModalComponent implements OnInit {

  form: FormGroup;
  submitted = false;
  editreponse: Object;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<EditConvParamModalComponent>,
              private api: ApiProvider,
              private fb: FormBuilder,
              private toast: Toaster) { }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

 
  ngOnInit() {
    this.initForm();
  }


  initForm(): void {
    this.form = this.fb.group({
      place: [this.data[0].nombrePlaces, [Validators.required, Validators.pattern(placepattern)]],
      franchise: [this.data[0].montantFranchiseMinimum, [Validators.required, Validators.pattern(valeurPattern)]],
      libelle: [this.data[0].libelle, Validators.required],
      invalidite: [this.data[0].invalidite, [Validators.required, Validators.pattern(valeurPattern)]],
      frais: [this.data[0].fraisMedicaux, [Validators.required, Validators.pattern(valeurPattern)]],
      deces: [this.data[0].deces, [Validators.required, Validators.pattern(valeurPattern)]],
      cnpac: [this.data[0].cnpac, [Validators.required, Validators.pattern(valeurPattern)]],
      accesoires: [this.data[0].accessoires, [Validators.required, Validators.pattern(valeurPattern)]],
    });
  }
  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return (this.submitted && this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }

  editConventionParam(){
  
  if(this.form.valid){
    let that = this;
    this.api.editConvParam(this.form.value, this.data[1]).subscribe(
      rep =>{
        that.editreponse = rep;
        this.closeDialog(this.dialogRef);
        this.toast.showSuccessMessage("Paramètre Modifié");

      },
      err => {
        this.toast.showToastmessageBack("Modification erronée");
        
      }
    );
  }
  this.submitted = true;
  }

}
