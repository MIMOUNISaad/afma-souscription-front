import { AppComponent } from './../../root/app.component';
import { ApiProvider } from './../../providers/api.provider';
import { Component, Input, OnChanges, SimpleChanges, Output, EventEmitter, Injectable, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { MatDialog } from '@angular/material';
import {ConventionComponent, PersonneTransporteComponent, TierceComponent} from './../index';

const phonePattern = '^0[0-9]{9}$';
const nomPattern = "^[a-z A-Z]+$";
const emailPattern = '^[\.a-zA-Z0-9_-]+@[a-zA-Z0-9-]+[\.]{1}[\.a-zA-Z0-9-]+$';
const datePattern = "^(((0[1-9]|[12][0-9]|3[01])/(0[13578]|1[02])/((19|[2-9][0-9])[0-9]{2}))|((0[1-9]|[12][0-9]|30)/(0[13456789]|1[012])/((19|[2-9][0-9])[0-9]{2}))|((0[1-9]|1[0-9]|2[0-8])/02/((19|[2-9][0-9])[0-9]{2}))|(29/02/((1[6-9]|[2-9][0-9])(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$";
const cinPattern = '^[a-zA-Z]{1,3}[0-9]+$'

@Injectable()
@Component({
  selector: 'app-driver-info',
  templateUrl: './driver-info.component.html',
  styleUrls: ['./driver-info.component.css']
})
export class DriverInfoComponent implements OnChanges {

  @Input('toggle') toggle: boolean;
  @Output() onNext = new EventEmitter<any>();
  @Output('withConvention') withConvention:boolean ;
  form: FormGroup;
  submitted = false;
  birthday: null;
  devis: any;
  fileName: any;
  compagnies = [
    {
      label: 'BMCI',
      value: 'BMCI'
    }
  ];

  constructor(
    private fb: FormBuilder,
    private api: ApiProvider,
    public dialog: MatDialog,
    public rooot: AppComponent
  ) {
    this.initForm();
    this.subscribeToFormChanges();
  }

  initForm(): void {
    this.form = this.fb.group({
      captcha:['',Validators.required],
      nom: [null, [Validators.required, Validators.pattern(nomPattern)]],
      prenom: [null, [Validators.required, Validators.pattern(nomPattern)]],
      cin: [null, [Validators.required, Validators.minLength(4), Validators.pattern(cinPattern)]],
      gsm: [null, [Validators.required, Validators.pattern(phonePattern)]],
      email: [null, [Validators.required, Validators.pattern(emailPattern)]],
      adresse: [null, Validators.required],
      date1: [null, [Validators.required, this.validDate1]],
      date2: [null, [Validators.required, this.validDate2]],
      birthday: [null],
      dop: [null],
      typeDevis: [null],
      convention: [null],
      compagnie: [null]
    });
  }


  validDate1(c: FormControl) {
    if(c.value && c.value.length === 8) {
      let date = c.value.slice(0, 2) + '/' + c.value.slice(2);
      date = date.slice(0, 5) + '/' + date.slice(5);
      if(RegExp(datePattern).test(date)) {
        let dateArray = date.split('/');
        let d = new Date(parseInt(dateArray[2]), parseInt(dateArray[1]) - 1, parseInt(dateArray[0])).setHours(0,0,0,0);
        let today = new Date().setHours(0,0,0,0);
        if (d < today) {
          c.root.get('birthday').setValue(d);
          return null;
        } else {
          return {
            ValidateDate: true
          };
        }
      } else {
        return {
          ValidateDate: true
        };
      }
    } else {
      return {
        ValidateDate: true
      };
    }
  }

  validDate2(c: FormControl) {
    if(c.value && c.value.length === 8) {
      let date = c.value.slice(0, 2) + '/' + c.value.slice(2);
      date = date.slice(0, 5) + '/' + date.slice(5);
      if(RegExp(datePattern).test(date)) {
        let dateArray = date.split('/');
        let d = new Date(parseInt(dateArray[2]), parseInt(dateArray[1]) - 1, parseInt(dateArray[0])).setHours(0,0,0,0);
        let today = new Date().setHours(0,0,0,0);
        let birthday = new Date(c.root.get('birthday').value).setHours(0,0,0,0);
        if(d < today) {
          if(birthday) {
            if(d > birthday) {
              c.root.get('dop').setValue(d);
              return null;
            } else {
              return {
                ValidateDate: true
              };
            }
          } else {
            c.root.get('dop').setValue(d);
            return null;
          }
        } else {
          return {
            ValidateDate: true
          };
        }
      } else {
        return {
          ValidateDate: true
        };
      }
    } else {
      return {
        ValidateDate: true
      };
    }
  }

  subscribeToFormChanges() {
    let that = this;
    const myFormValueChanges$ = that.form.valueChanges;
    myFormValueChanges$.subscribe(function(event) {
      if(that.birthday !== event.date1) {
        that.birthday = event.date1;
        that.form.get('date2').setValue(that.form.get('date2').value);
      }
    });
  }

  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return (this.submitted && this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }

  ngOnChanges(changes: SimpleChanges) {
    let that = this;
    if (changes['toggle'] && !changes['toggle'].firstChange) {
      if(this.form.value.typeDevis === 'true' && this.form.valid) {
        that.onNext.emit({
          valid: this.form.valid,
          value: this.form.value
        });
        this.api.withConvention=true;
        this.withConvention=true;
      } else {
        this.onNext.emit({
          valid: this.form.valid,
          value: this.form.value
        });
        this.withConvention=false;
        this.api.withConvention=false;
      }
    }
  }
}
