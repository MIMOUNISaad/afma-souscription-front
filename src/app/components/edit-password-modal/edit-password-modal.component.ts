import { Toaster } from 'src/app/providers/toast/toast';

import { Component, OnInit, Inject } from '@angular/core';
import { ApiProvider } from 'src/app/providers/api.provider';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-edit-password-modal',
  templateUrl: './edit-password-modal.component.html',
  styleUrls: ['./edit-password-modal.component.css']
})
export class EditPasswordModalComponent implements OnInit {
 
  form: FormGroup;
  hide = true;
  
  submitted = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<EditPasswordModalComponent>,
              private api: ApiProvider,
              private fb: FormBuilder,
              private toast: Toaster) { }

  closeDialog(value) {
    this.dialogRef.close(value);
  }


  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      currentpassword: [null, Validators.required],
      newpassword: [null, Validators.required],
      newpassword2: [null, [Validators.required, this.comparenewPasswords]]
    });
  }
  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return (this.submitted && this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }
  comparenewPasswords(c: FormControl) { 
      if(c.value){
        let newpassword = c.root.get('newpassword').value;
        let newpasswordconfirm = c.root.get('newpassword2').value;
        if(newpassword !== newpasswordconfirm ) {
        return {
          validation: true};
        }
      }
    }
  validerPassword(){
    if (this.form.valid) {
      let that = this;
      this.api.editPassword(this.f.newpassword2.value, this.f.newpassword.value, this.f.currentpassword.value).subscribe(
        res => {
          this.data = res;
          that.toast.showSuccessMessage("la modification bien effectuée ");
          that.closeDialog(this.dialogRef);
        },
        err => {
          if(err.error.code === 301)
          that.toast.showToastmessageBack("Votre mot de passe incorrect!");
        }
      );
    }
    this.submitted =true;
}
}
