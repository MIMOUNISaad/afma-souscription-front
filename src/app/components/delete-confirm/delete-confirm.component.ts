import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-delete-confirm',
  templateUrl: './delete-confirm.component.html',
  styleUrls: ['./delete-confirm.component.css']
})
export class DeleteConfirmComponent{

  constructor(@Inject(MAT_DIALOG_DATA) public message: string,
              public dialogRef: MatDialogRef<DeleteConfirmComponent>
    ) { }

    onNoClick(){
    this.dialogRef.close();
    
  }


}
