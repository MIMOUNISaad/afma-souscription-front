import { Toaster } from 'src/app/providers/toast/toast';
import { Component, OnInit, Inject } from '@angular/core';
import { ApiProvider } from 'src/app/providers/api.provider';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddUserModalComponent, EditUserModalComponent, EditPasswordModalComponent, DeleteConfirmComponent } from '..';




@Component({
  selector: 'app-gerer-users',
  templateUrl: './gerer-users.component.html',
  styleUrls: ['./gerer-users.component.css']
})


export class GererUsersComponent implements OnInit {

  users: any;
  agence: '';
  agences: any;
  form: FormGroup;
  reponse: any;
  id: any;
  taille: any;
  constructor(
              public dialog: MatDialog,
              private readonly api: ApiProvider,
              private auth: AuthService,
              private router: Router,
              private fb: FormBuilder,
              private toast: Toaster
    ) {
    this.initForm();
    }

    initForm(): void {
      this.form = this.fb.group({
        agence: [1, Validators.required]
      });
    }
    get f() { return this.form.controls; }

    hasError(attr): boolean {
      return  this.form.controls[attr].invalid;
    }

    onChange(agence) {
      this.showUsersByagences();
  }
    AddUserFunction() {
      const dialogRef = this.dialog.open(AddUserModalComponent,  {
        data: this.f.agence.value,
        height: '',
        width: '40%',
      });
      dialogRef.afterClosed().subscribe(result => {
        this.showUsersByagences();      });
    }

  ngOnInit() {
    this.getAgences();
    this.api.getUsersByAgences(this.f.agence.value).subscribe(
        res => {
          this.users = res;
          this.taille = this.users.length;
          },
        err => {
          this.toast.showToastmessageBack('Erreur lors de la récupération des Utilisateurs !');
        }
      )
    
  }

  getAgences() {
    let that = this;
    this.api.getAgences().subscribe(
      res => {
        that.agences = res;
        
      },
      err => {

      }
    );
  }
  showUsersByagences(){
    this.api.getUsersByAgences(this.f.agence.value).subscribe(
      res => {
        this.users = res;
        this.taille = this.users.length;
      },
      err => {
        this.toast.showToastmessageBack("Erreur lors de la récupération des Utilisateurs !")
            }
    )
  }
  


EditUserFunction(user) {
    const that = this;
    const dialogRef = this.dialog.open(EditUserModalComponent, {
        data: user,
        height: '',
        width: '40%'
      });

    dialogRef.afterClosed().subscribe(result => {
      this.showUsersByagences();
      });
}
 
deleteUser(id) {
  const dialogRef = this.dialog.open(DeleteConfirmComponent, {
    width: '350px',
    data: "Supprimer?"
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result) {
      let that = this;
      this.api.deletUser(id).subscribe(
        res => {
          that.reponse = res;
          that.toast.showSuccessMessage("Suppression effectuée  ");
          this.showUsersByagences();
        },
        err => {
          that.toast.showToastmessageBack(err.error.message);
        }
      );
    }
  });
}
}
