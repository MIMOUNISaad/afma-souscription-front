import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-collision',
  templateUrl: './collision.component.html',
  styleUrls: ['./collision.component.css']
})
export class CollisionComponent {

  form: FormGroup;
  submitted = false;

  collisionOptions = [{
    label: '10 000',
    value: 1
  }, {
    label: '20 000',
    value: 2
  }, {
    label: '30 000',
    value: 3
  }, {
    label: '40 000',
    value: 4
  }, {
    label: '50 000',
    value: 5
  }];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<CollisionComponent>,
    private fb: FormBuilder,
  ) {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      code: [this.data],
      option: [1, Validators.required]
    });
  }

  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return this.submitted && (this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }

  validForm() {
    return this.f.option.valid;
  }

  closeDialog(value) {
    this.dialogRef.close(parseInt(value));
  }
}
