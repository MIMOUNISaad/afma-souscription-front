import { Component, Input } from '@angular/core';
import { ApiProvider } from 'src/app/providers/api.provider';

@Component({
  selector: 'app-recapitulatif',
  templateUrl: './recapitulatif.component.html',
  styleUrls: ['./recapitulatif.component.css']
})
export class RecapitulatifComponent {

  @Input('data') data: any;

  usageOptions = [{
    'libelle': 'Tourisme',
    'code': 'A'
  }];

  carburantOptions = [{
    label: 'Diesel',
    value: 'D'
  }, {
    label: 'Essence',
    value: 'E'
  }];

  constructor(
    private api: ApiProvider
  ) { }

  getUsage(code) {
    return code ? this.usageOptions.filter(u => u.code === code)[0]['libelle'] : null;
  }

  getCarburant(value) {
    return value ? this.carburantOptions.filter(c => c.value === value)[0]['label'] : null;
  }

  downloadPDF() {
    let primeAnnuelleHT = 0;

    this.data.taxes.responseGaranties.forEach(taxe => {
      primeAnnuelleHT += taxe.prime;
    });

    this.api.DownloadDevis(this.data.conducteur, this.data.vehicule, this.data.garanties, this.data.taxes, this.data.garanties['sommeGaranties'], primeAnnuelleHT, this.data.crm).subscribe(
      res => {
        const blob = new Blob([res.body], { type: 'application/octet-stream' });

        let url = window.URL.createObjectURL(blob);
        let a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = 'Devis.pdf';
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
      },
      err => {
        // this.toast.showError();
      }
    );

  }

}

