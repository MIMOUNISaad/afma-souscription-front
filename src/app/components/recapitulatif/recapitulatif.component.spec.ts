import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecapitulatifComponent } from './recapitulatif.component';

import { NgModule,NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('RecapitulatifComponent', () => {
  let component: RecapitulatifComponent;
  let fixture: ComponentFixture<RecapitulatifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecapitulatifComponent ],
      schemas:[NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecapitulatifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
