import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssuranceAutomobileComponent } from './assurance-automobile.component';

describe('AssuranceAutomobileComponent', () => {
  let component: AssuranceAutomobileComponent;
  let fixture: ComponentFixture<AssuranceAutomobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssuranceAutomobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssuranceAutomobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
