import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConditionsSpecialComponent } from './conditions-special.component';

describe('ConditionsSpecialComponent', () => {
  let component: ConditionsSpecialComponent;
  let fixture: ComponentFixture<ConditionsSpecialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConditionsSpecialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditionsSpecialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
