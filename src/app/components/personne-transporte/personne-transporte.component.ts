import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-personne-transporte',
  templateUrl: './personne-transporte.component.html',
  styleUrls: ['./personne-transporte.component.css']
})
export class PersonneTransporteComponent {

  form: FormGroup;
  submitted = false;

  personneTransporteOptions = [{
    label: '20 000 (gratuite)',
    value: 1
  }, {
    label: '30 000',
    value: 2
  }, {
    label: '40 000',
    value: 3
  }, {
    label: '50 000',
    value: 4
  }, {
    label: '60 000',
    value: 5
  }, {
    label: '70 000',
    value: 6
  }, {
    label: '80 000',
    value: 7
  }, {
    label: '100 000',
    value: 8
  }, {
    label: '120 000',
    value: 9
  }];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PersonneTransporteComponent>,
    private fb: FormBuilder,
  ) {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      code: [this.data],
      option: [1, Validators.required]
    });
  }

  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return this.submitted && (this.form.controls[attr].invalid) || (this.form.controls[attr].touched && this.form.controls[attr].invalid);
  }

  validForm() {
    return this.f.option.valid;
  }

  closeDialog(value) {
    this.dialogRef.close(parseInt(value));
  }
}
