import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GererConventionComponent } from './gerer-convention.component';

describe('GererConventionComponent', () => {
  let component: GererConventionComponent;
  let fixture: ComponentFixture<GererConventionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GererConventionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GererConventionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
