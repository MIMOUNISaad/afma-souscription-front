import { DeleteConfirmComponent } from './../delete-confirm/delete-confirm.component';
import { Component, OnInit } from '@angular/core';
import { ApiProvider } from 'src/app/providers/api.provider';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AddUserModalComponent, EditUserModalComponent, AddConventionModalComponent, EditConventionModalComponent } from '..';
import { Toaster } from 'src/app/providers/toast/toast';

@Component({
  selector: 'app-gerer-convention',
  templateUrl: './gerer-convention.component.html',
  styleUrls: ['./gerer-convention.component.css']
})
export class GererConventionComponent implements OnInit {
  
  agence: '';
  form: FormGroup;
  parteners: any;
  partnerDefault: '';
  conventions: any;
  reponse: any;
  taille: any;

  pagination: any = {
    page: 0
  };

  constructor(
    public dialog: MatDialog,
    private readonly api: ApiProvider,
    private router: Router,
    private fb: FormBuilder,
    private toast: Toaster
    ) {
    }

  ngOnInit() {
    this.api.myuserconnected = localStorage.getItem('Iduserconnect');
    let myuserconnected = localStorage.getItem('Iduserconnect');

    if(myuserconnected === null) {
      this.router.navigate(['/manager']);      //////////////////////**********/////// */
    } else {
      this.getPartener();
    }
  }
  initForm(): void {
    
    this.form = this.fb.group({
      partener: [this.parteners[0].partenaireId]
    });
    this.showConventionByPartener(0,10);
  }
  get f() { return this.form.controls; }

  hasError(attr): boolean {
    return this.form.controls[attr].invalid;
  }
  getPartener(){
    let that = this;
    this.api.getPartener().subscribe(
      res => {
        that.parteners = res;
        that.initForm();                                 ///////**************///////******* */
      },
      err => {
        that.toast.showToastmessageBack(err.error.message);
      }
    );
  }

  showConventionByPartener(page, pageSize){
    
    let that = this;
    this.api.getConventionByPartener(this.f.partener.value,0,10).subscribe(
      res => {
        that.conventions = res['content'];
        this.taille = this.conventions.length;
        this.pagination.collectionSize = res['totalElements'];
        this.pagination.pageSize = res['size'];
      },
      err => {
        that.toast.showToastmessageBack("Erreur lors de la récupération des conventions !");
            }
    );
  }
  getNext(event){
    this.pagination.page = event.pageIndex;

    this.conventions = [];
    this.showConventionByPartener(this.pagination.page,10);
  }

  AddConventionFunction() {
    const dialogRef = this.dialog.open(AddConventionModalComponent,  {
      data: this.f.partener.value,
      height: '',
      width: '40%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.showConventionByPartener(0,10);
      }
    });
  }
  deleteConventions(id) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent, {
      width: '350px',
      data: "Supprimer?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        let that = this;
        this.api.deleteConvention(id).subscribe(
          res => {
            that.reponse = res;
            that.toast.showSuccessMessage("Suppression effectuée  ");
            this.showConventionByPartener(0,10);
          },
          err => {
            that.toast.showToastmessageBack("erreur de suppression ! ");

          }
        );
      }
    });
  }


EditConventionFunction(convention) {
  const that = this;
  const dialogRef = this.dialog.open(EditConventionModalComponent, {
      data: [this.f.partener.value, convention],
      height: '',
      width: '40%'
    });

  dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.showConventionByPartener(0,10);
      }
    });

  }
}
 // showAllConventions(){
  //   this.api.getConventionByPartener(partener).subscribe(
  //     res => {
  //       that.cons = res;
  //     },
  //     er => {
  //       this.toast.
  //     }
  //   )
  // }
