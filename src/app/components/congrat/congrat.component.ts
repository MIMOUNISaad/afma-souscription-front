import { Router } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-congrat',
  templateUrl: './congrat.component.html',
  styleUrls: ['./congrat.component.css']
})
export class CongratComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<CongratComponent>) { }

  ngOnInit() {
  }



}
