import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { EMPTY, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from './auth/auth.service';



@Injectable()
export class InterceptorProvider implements HttpInterceptor {

    token: string;

    constructor(
        private authProvider: AuthService,
        private router: Router
    ) {
        this.token = null;
        this.subscribeToAuthProvider();
    }

   
    subscribeToAuthProvider(): void {
        this.authProvider.authUser.subscribe(
          token => {
            this.token = token;
          }
        );
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


        if (this.token) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + this.token) });
        }

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                return event;
            }),
            catchError((err: HttpErrorResponse) => {
                if (err.status === 403) {
                    this.router.navigate(['login']);
                }
                return throwError(err);
            }),

        );
    }
}
