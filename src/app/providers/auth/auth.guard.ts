import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  token: string;

  constructor(
    private authService: AuthService, private router: Router
  ) {
    this.token = null;
    this.subscribeToAuthProvider();
  }

  subscribeToAuthProvider(): void {
    this.authService.authUser.subscribe(
      token => {
        this.token = token;
      }
    );
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      if (this.token) {
          return true;
      } else {
        this.router.navigate(['/manager']);
        return false;
      }
  }
}
