
import { Injectable, Output } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { ApiProvider } from '../api.provider';
import { Toaster } from './../toast/toast';
import { ReplaySubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedIn: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  private ErrorConnect: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  loginId: void;
  role: void;
  authUser = new ReplaySubject<any>(1);



  get isErrorLogin() {
    return this.ErrorConnect.asObservable();
  }
  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  constructor(
    private api: ApiProvider,
    private router: Router,
    private toast: Toaster
  ) { }

  login(data) {
    let that = this;

    let user = 'Iduserconnect';
    let userRole = 'userRole'


    this.api.signin(data).subscribe(
      res => {
        //that.loggedIn.next(res['utilisateurId']);
        that.loggedIn.next(res['role']);
        let user = 'Iduserconnect';
        let userRole = 'userRole'
        that.loggedIn.next("user");
        this.authUser.next(res['token']);
        this.loginId = localStorage.setItem(user, res['utilisateurId']);
        this.role = localStorage.setItem(userRole, res['role']);
        

    
  
        
        if(res['role'] === 'admin')
        {
          that.router.navigate(['/admin-management/users']);
        }
        else if(res['role'] === 'user'){
          that.router.navigate(['/management']);
        }else{
          that.router.navigate(['/index']);
        }
      },
      err => {
        if(err['code'] == 401){
          this.toast.showToastmessageBack("Captcha incorrect !"); 
        } 
         this.toast.showToastmessageBack("Login ou mot de passe incorrect !");
         //that.ErrorConnect.next("Login ou mot de passe incorrect!")
      }
    );
  }

  logout() {
    this.loggedIn.next(null);
    this.ErrorConnect.next(null);
    this.authUser.next(null);
    this.router.navigate(['/manager']);
    localStorage.removeItem('Iduserconnect');
    localStorage.removeItem('userRole');
  }
}
