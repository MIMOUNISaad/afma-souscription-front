import {Pipe, PipeTransform} from '@angular/core';

// pour la transformations des numbers-fromat a afficher
@Pipe({
  name: 'pricePipe'
})
export class PricePipe implements PipeTransform {

  transform(val: string) {
    if(!val){
      return 0;
    }

    const splited = val.split(',');
    let FinalValue = '';
    for(let s of splited){
      FinalValue += s + ' ';
    }

    return FinalValue;
    
  }

}
