import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { APP_CONFIG } from './../app.config';
import { debug } from 'util';

@Injectable()
export class ApiProvider {

  withConvention: boolean;
  myuserconnected: any;
  url: any;
  roleConnected: string;
  constructor(
    @Inject(APP_CONFIG) private readonly config: any,
    private readonly http: HttpClient
  ) {
    this.myuserconnected = localStorage.getItem('Iduserconnect');
    this.roleConnected = localStorage.getItem('userRole');

    this.url = this.config.server_url;
  }

  getGaranties(typeDevis, dateMEC, glace, cinRc): any {
    let body = {
      withConvention: typeDevis,
      dateMEC: new Date(dateMEC).toISOString().split('T')[0],
      valeurGlace: parseFloat(glace),
      cinRc: cinRc

    };

    return this.http.post(`${this.url}garanties`, body);
  }

  getCrm(imma, fullName) {
    let body = {
      immatriculation: imma,
      assureName: fullName,
    };
    return this.http.post(`${this.url}CRM`, body);
  }

  getTaxes(carInfo, garanties, cin, typeDevis, crm) {
    let body = {
      cinRc: cin,
      dateDebutCouverture: new Date(carInfo._debutCouvertue).toISOString().split('T')[0],
      dateFinCouverture: new Date(carInfo._finCouverture).toISOString().split('T')[0],
      dateMec: new Date(carInfo._dateMEC).toISOString().split('T')[0],
      energie: carInfo.carburant,
      garanties: garanties.map(gar => {
        return {
          code: gar.code,
          option: gar.option
        };
      }),
      horsePower: parseInt(carInfo.puissance),
      usage: carInfo.usage,
      valeurGlace: parseFloat(carInfo.glace),
      valeurNeuf: parseFloat(carInfo.neuf),
      valeurVenale: parseFloat(carInfo.venale)
    };
    return this.http.post(`${this.url}decomptePrime?withConvention=${typeDevis}&crm=${crm}`, body);
  }

  saveDevis(driverInfo, carInfo, agence, justificatifs) {

    let body = {
      agenceId: agence.city.agenceId,
      devisDTO: {
        dateDebutCouverture: new Date(carInfo._debutCouvertue).toISOString().split('T')[0],
        dateFinCouverture: new Date(carInfo._finCouverture).toISOString().split('T')[0]
      },
      assureDTO: {
        name: driverInfo.nom.toUpperCase() + ' ' + driverInfo.prenom.toUpperCase(),
        email: driverInfo.email,
        adress: driverInfo.adresse,
        phoneNumber: driverInfo.gsm,
        cinRc: driverInfo.cin.toUpperCase(),
        birthDate: new Date(driverInfo.birthday).toISOString().split('T')[0],
        driverLicense: new Date(driverInfo.dop).toISOString().split('T')[0],
      },
      vehiculeDTO: {
        marque: carInfo.marque.toUpperCase(),
        immatriculation: carInfo.typeImma === 'ww' ? carInfo.immatriculationWw.toUpperCase() : carInfo.immatriculationDefinitf.code + '' + carInfo.immatriculationDefinitf.letter.toUpperCase() + '' + carInfo.immatriculationDefinitf.ville,
        energie: carInfo.carburant,
        horsePower: parseInt(carInfo.puissance),
        driver: driverInfo.nom,
        crm: null,
        valeurVenale: parseFloat(carInfo.venale),
        valeurNeuf: parseFloat(carInfo.neuf),
        valeurGlace: parseFloat(carInfo.glace),
        usage: carInfo.usage,
        driverLicenseDate: new Date(driverInfo.dop).toISOString().split('T')[0],
        dateMiseEnCirculation: new Date(carInfo._dateMEC).toISOString().split('T')[0],
        mecdate: null
      },
      justificationDTO: justificatifs.map(gar => {
        return {
          image: gar.file
        };
      }),

    };

    return this.http.post(`${this.url}devis`, body);
  }

  checkCin(cin) {
    let params = new HttpParams().set('cin', cin);
    return this.http.get(`${this.url}conventionInfos`, { params: params })
  }

  getValeurVenale(horsePower, valeurNeuf, dateMEC) {

    let params = new HttpParams().set('horsePower', horsePower)
      .set('valeurNeuf', valeurNeuf)
      .set('dateMEC', dateMEC);

    return this.http.get(`${this.url}valeurVenale`, { params: params })
  }

  getAgences() {
    return this.http.get(`${this.url}agences`);
  }

  DownloadDevis(driverInfo, carInfo, garanties, taxes, primeHT, primeAnnuelleHT, crm) {
    let body = {
      accessoires: taxes.decomptePrime.accessoires,
      assureDTO: {
        adress: driverInfo.adresse,
        birthDate: new Date(driverInfo.birthday).toISOString().split('T')[0],
        cinRc: driverInfo.cin.toUpperCase(),
        driverLicense: new Date(driverInfo.dop).toISOString().split('T')[0],
        email: driverInfo.email,
        name: driverInfo.nom.toUpperCase() + ' ' + driverInfo.prenom.toUpperCase(),
        phoneNumber: driverInfo.gsm
      },
      cnpac: taxes.decomptePrime.cnpac,
      dateDebutCouverture: new Date(carInfo._debutCouvertue).toISOString().split('T')[0],
      dateFinCouverture: new Date(carInfo._finCouverture).toISOString().split('T')[0],
      primeAnnuelleHT: primeAnnuelleHT,
      primeAutoPTATotale: taxes.decomptePrime.primeAutoPTATotale,
      primeRCAVAtotale: taxes.decomptePrime.primeRCAVAtotale,
      primeTotaleTTC: taxes.decomptePrime.primeTotaleTTC,
      primeprorataAutoPTA: taxes.decomptePrime.primeprorataAutoPTA,
      primeprorataRCAVA: taxes.decomptePrime.primeprorataRCAVA,

      selectedGaranties: taxes.responseGaranties,
      taxeProrataAutoPTA: taxes.decomptePrime.taxeProrataAutoPTA,
      taxeProrataRCAVA: taxes.decomptePrime.taxeProrataRCAVA,
      vehiculeDTO: {
        crm: null,
        dateMiseEnCirculation: new Date(carInfo._dateMEC).toISOString().split('T')[0],
        driverLicenseDate: new Date(driverInfo.dop).toISOString().split('T')[0],
        energie: carInfo.carburant,
        horsePower: parseInt(carInfo.puissance),
        immatriculation: carInfo.typeImma === 'ww' ? carInfo.immatriculationWw.toUpperCase() + 'WW' : carInfo.immatriculationDefinitf.code + '' + carInfo.immatriculationDefinitf.letter.toUpperCase() + '' + carInfo.immatriculationDefinitf.ville,
        marque: carInfo.marque.toUpperCase(),
        usage: carInfo.usage,
        valeurGlace: parseFloat(carInfo.glace),
        valeurNeuf: parseFloat(carInfo.neuf),
        valeurVenale: parseFloat(carInfo.venale),
      }
    };


    return this.http.post(`${this.url}devisBulletin?crm=${crm}&withConvention=${driverInfo.typeDevis}`, body, { observe: 'response', responseType: 'blob' });

  }

  signin(data) {
    return this.http.post(`${this.url}authenticate`, data);
  }

  showAllDevis(page, pageSize) {
    this.myuserconnected = localStorage.getItem('Iduserconnect');
    this.roleConnected = localStorage.getItem('userRole');
    return this.http.get(`${this.url}devisInvalid?page=${page}&size=${pageSize}&utilisateurId=${this.myuserconnected}`);
  }

  ValiderDevis(idDevis) {
    return this.http.get(`${this.url}validerDevis?devisId=${idDevis}`);
  }

  getUsers() {
    return this.http.get(`${this.url}users`);
  }

  editPassword(pass1, pass2, pass22) {
    let body = {
      newPassword: pass1,
      newPasswordConfirmation: pass2,
      oldPassword: pass22,
      utlisateurId: this.myuserconnected
    }
    return this.http.post(`${this.url}changePassword`, body);
  }

  getUsersByAgences(agence) {
    return this.http.get(`${this.url}usersByAgence?utilisateurId=${this.myuserconnected}&agenceId=${agence}`);
  }

  addUser(user, agence) {
    let body = {
      nom: user.nom,
      prenom: user.prenom,
      matricule: user.matricule,
      login: user.login,
      password: user.password,
      agenceId: user.agence
    }
    return this.http.post(`${this.url}saveUser?adminId=${this.myuserconnected}`, body);
  }

  deletUser(userID) {
    return this.http.get(`${this.url}deleteUser?adminId=${this.myuserconnected}&utilisateurId=${userID}`);

  }
  editUser(user) {
    let body = {
      agenceId: user.agence,
      login: user.login,
      matricule: user.matricule,
      nom: user.nom,
      prenom: user.nom,
      role: "user",
      utilisateurId: user.utilisateurId
    }
    return this.http.post(`${this.url}updateUser?adminId=${this.myuserconnected}`, body);

  }

  getPartener() {
    return this.http.get(`${this.url}partenaires?adminId=${this.myuserconnected}`);

  }
  getConventionByPartener(partener, page, pageSize) {
    return this.http.get(`${this.url}conventionsByPartenaire?page=${page}&size=${pageSize}&partenaireId=${partener}&utilisateurId=${this.myuserconnected}`);
  }
  getInfoGaranties() {
    return this.http.get(`${this.url}garantiesInfo`);
  }
  addConvention(convention, partener) {
    let body = {
      code: convention.code,
      configuration: convention.configuration,
      conventionId: null,
      franchise: convention.franchise,
      prime: convention.prime,
      taux: convention.taux
    }
    return this.http.post(`${this.url}saveConvention?partenaireId=${partener}&utilisateurId=${this.myuserconnected}`, body);

  }

  deleteConvention(convention) {
    return this.http.get(`${this.url}deleteConvention?conventionId=${convention}&utilisateurId=${this.myuserconnected}`);
  }

  editConvention(convention, partenaire, IdConvention) {
    let body = {
      code: convention.code,
      configuration: convention.configuration,
      conventionId: IdConvention,
      franchise: convention.franchise,
      prime: convention.prime,
      taux: convention.taux
    }
    // tslint:disable-next-line: max-line-length
    return this.http.post(`${this.url}updateConvention?partenaireId=${partenaire}&utilisateurId=${this.myuserconnected}&conventionId=${IdConvention}`, body);
  }

  getConventionParam(partener) {
    return this.http.get(`${this.url}ConventionParams/Partenaire/${partener}?utilisateurId=${this.myuserconnected}`);
  }

  getPartenaireNotConvParam() {
    return this.http.get(`${this.url}partenaire?adminId=${this.myuserconnected}`);
  }
  addConventionParam(convParam, partener) {
    let body = {
      accessoires: convParam.accesoires,
      cnpac: convParam.cnpac,
      conventionParamId: null,
      deces: convParam.deces,
      fraisMedicaux: convParam.frais,
      invalidite: convParam.invalidite,
      libelle: convParam.libelle,
      montantFranchiseMinimum: convParam.franchise,
      nombrePlaces: convParam.place
    }
    return this.http.post(`${this.url}ConventionParams/Partenaire/${partener}?utilisateurId=${this.myuserconnected}`, body);

  }

  deleteConvParam(idConvParam) {
    return this.http.delete(`${this.url}ConventionParams/${idConvParam}?utilisateurId=${this.myuserconnected}`);
  }

  editConvParam(convParam, partener) {

    let body = {
      accessoires: convParam.accesoires,
      cnpac: convParam.cnpac,
      conventionParamId: null,
      deces: convParam.deces,
      fraisMedicaux: convParam.frais,
      invalidite: convParam.invalidite,
      libelle: convParam.libelle,
      montantFranchiseMinimum: convParam.franchise,
      nombrePlaces: convParam.place
    }
    return this.http.put(`${this.url}ConventionParams/Partenaire/${partener}?utilisateurId=${this.myuserconnected}`, body);

  }
}
