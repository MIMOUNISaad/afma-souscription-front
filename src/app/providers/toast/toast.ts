import { ToastrManager } from 'ng6-toastr-notifications';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class Toaster {
  
    constructor(public toastr: ToastrManager) {}
  
    showSuccess() {
      this.toastr.successToastr('This is success toast.', 'Success!');
  }
  showSuccessMessage(msg) {
    this.toastr.successToastr(msg, 'Succès!');
}


  showError() {
      this.toastr.errorToastr('Problème technique! réessayer ultérieurement.', 'Oops!');
  }
  
  showWarning() {
      this.toastr.warningToastr('This is warning toast.', 'Alert!');
  }
  
  showInfo() {
      this.toastr.infoToastr('This is info toast.', 'Info');
  }
  
  showCustom() {
      this.toastr.customToastr(
      `<span style='color: green; font-size: 16px; text-align: center;'>Custom Toast</span>`,
      null,
      { enableHTML: true }
      );
  }
  
  showToastRed(msg, position: any = 'top-center', animate: any = 'slideFromTop', ) {
    this.toastr.errorToastr(msg, 'Oops!', {
        position: position,
        animate: animate
    });
}
  showToast(position: any = 'top-center', animate: any = 'slideFromTop', ) {
      this.toastr.errorToastr('Problème technique! réessayer ultérieurement.', 'Oops!', {
          position: position,
          animate: animate
      });
  }

  showToastmessageBack(msg, position: any = 'top-right', animate: any = 'slideFromTop', ) {
    this.toastr.errorToastr(msg, 'Erreur', {
        position: position,
        animate: animate
    });
}
  
  }
  