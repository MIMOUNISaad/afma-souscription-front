import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ApiProvider } from './providers/api.provider';
import { APP_CONFIG, AppConfig } from './app.config';
import { PricePipe } from './providers/PricePipe ';
import { AuthGuard } from './providers/auth/auth.guard';
import { InterceptorProvider } from './providers/http.provider';
import { HTTP_INTERCEPTORS } from '@angular/common/http';



export const PROVIDERS : any[] = [
  ApiProvider, PricePipe,AuthGuard,
  {provide: LocationStrategy, useClass: HashLocationStrategy},
  { provide: APP_CONFIG, useValue: AppConfig },
  { provide: HTTP_INTERCEPTORS, useClass: InterceptorProvider, multi: true }
];
