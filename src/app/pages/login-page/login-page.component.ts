import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
    if(localStorage.getItem('role')==='admin'){
      this.router.navigate["/admin-management"];
    }
    else  if(localStorage.getItem('role')==='user'){
      this.router.navigate["/management"];
    }
  }

}
