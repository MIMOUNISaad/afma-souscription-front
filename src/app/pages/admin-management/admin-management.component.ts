import { ApiProvider } from './../../providers/api.provider';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-management',
  templateUrl: './admin-management.component.html',
  styleUrls: ['./admin-management.component.css']
})
export class AdminManagementComponent implements OnInit {
  user: any;
  constructor(
    private readonly api: ApiProvider,
    private auth: AuthService,
    private router: Router
    ) {
      this.api.myuserconnected = localStorage.getItem('Iduserconnect');
      let myuserconnected = localStorage.getItem('Iduserconnect');
      let roleConnected = localStorage.getItem('userRole');
      if(myuserconnected === null || roleConnected !== 'admin') { 
        this.router.navigate(['/manager']);
      }
    
    }
    ngOnInit() {
      
      let that = this;                     // a utiliser pour l'affichage de l'user
      this.auth.isLoggedIn.subscribe(
        res => {
          that.user = res;
        }
      );
    }
}
