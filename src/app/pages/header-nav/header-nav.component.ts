import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { Router } from '@angular/router';
import { EditPasswordModalComponent } from 'src/app/components';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { ApiProvider } from 'src/app/providers/api.provider';


@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.css']
})
export class HeaderNavComponent implements OnInit {

  clicked: boolean;

item: number = 1;
  constructor(
        private auth: AuthService,
           ) {}

  ngOnInit() {
  }

  logout() {
    this.auth.logout();
  }
}
