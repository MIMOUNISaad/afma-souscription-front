export { DemanderDevisPageComponent } from './demander-devis-page/demander-devis-page.component';
export { LoginPageComponent } from './login-page/login-page.component';
export { MainPageComponent } from './main-page/main-page.component';
export { ManagementComponent } from './management/management.component';
export {HeaderNavComponent} from './header-nav/header-nav.component';
export {AdminManagementComponent} from './admin-management/admin-management.component';
