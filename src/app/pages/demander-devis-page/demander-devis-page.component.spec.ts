import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemanderDevisPageComponent } from './demander-devis-page.component';

describe('DemanderDevisPageComponent', () => {
  let component: DemanderDevisPageComponent;
  let fixture: ComponentFixture<DemanderDevisPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemanderDevisPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemanderDevisPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
