import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';

import { Component, Injectable } from '@angular/core';
import { ApiProvider } from './../../providers/api.provider';
import { CongratComponent } from 'src/app/components';
import { Toaster } from 'src/app/providers/toast/toast';

@Injectable()
@Component({
  selector: 'app-demander-devis-page',
  templateUrl: './demander-devis-page.component.html',
  styleUrls: ['./demander-devis-page.component.css']
})
export class DemanderDevisPageComponent {

  selectedStep: number = 1;
  data: any;
  garanties: any = null;
  devis: any = null;
  erreur: any = null;
  crm: any = null;
  payment: any = 'online';

  steps: any[] = [
    {
      title: 'Informations',
      toggle: false,
      value: null,
      index: 1
    },
    {
      title: 'Véhicule',
      toggle: false,
      value: null,
      index: 2
    },
    {
      title: 'Garanties',
      toggle: false,
      value: null,
      index: 3
    },
    {
      title: 'Récaputilatif',
      index: 4
    },
    {
      title: 'Justificatifs',
      toggle: false,
      value: null,
      index: 5
    },
    {
      title: 'Paiement',
      toggle: false,
      value: null,
      index: 6
    }
  ];

  constructor(
    private readonly api: ApiProvider,
    public dialog: MatDialog,
    private router: Router,
    private toast: Toaster
  ) { }

  show(index: number): boolean {
    return this.selectedStep === index;
  }

  next(): void {
    if (this.steps[this.selectedStep - 1].toggle !== undefined) {
      this.steps[this.selectedStep - 1].toggle = !this.steps[this.selectedStep - 1].toggle;
    } else {
      this.goto(this.selectedStep + 1);
    }
  }

  onNext(event): void {

    if (event.valid) {
      if (event.value !== undefined) {
        this.steps[this.selectedStep - 1].value = event.value;
      }


      if (this.selectedStep === this.steps.length) {
        this.api.saveDevis(this.steps[0].value, this.steps[1].value, this.steps[5].value, this.steps[4].value['justificatifsData']).subscribe(
          res => {
            this.devis = res;
          },
          err => {

            if (err.error.code === 409) {

            }
          }
        );
        if (this.steps[this.selectedStep - 1].value.paymentType === "cheque") {
          const dialogRef = this.dialog.open(CongratComponent);

          dialogRef.afterClosed().subscribe(result => {
            this.router.navigate(['/index'])
          });
        } else {
          const body = {
            'nom': this.steps[0].value.nom + ' ' + this.steps[0].value.prenom,
            'tel': this.steps[0].value.gsm,
            'email': this.steps[0].value.email,
            'total': this.steps[this.selectedStep - 1].value.total
          };

          this.paymentRequest('https://afma-online.ma/afma-online-payment/PaymentRequest', body);
        }
      } else {
        this.goto(this.selectedStep + 1);
      }
    }
  }

  paymentRequest(path, params, method = 'post') {

    const form = document.createElement('form');
    form.method = method;
    form.action = path;

    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        const hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = key;
        hiddenField.value = params[key];
        form.appendChild(hiddenField);
      }
    }

    document.body.appendChild(form);
    form.submit();
  }

  back(): void {
    this.goto(this.selectedStep - 1);
  }

  goto(index: number) {

    let that = this;
    if (index === 4) {
      this.getCRM();
      this.api.getTaxes(this.steps[1].value, this.steps[2].value['garantiesData'], this.steps[0].value.cin, this.steps[0].value.typeDevis, this.crm).subscribe(
        res => {

          that.steps[2].value.garantiesData.forEach((garantie, garantieIndex) => {
            garantie.valeurAssure = res['responseGaranties'][garantieIndex]['valeurAssure'];
            garantie.prime = res['responseGaranties'][garantieIndex].prime;
          });

          that.data = {
            crm: this.crm,
            conducteur: that.steps[0].value,
            vehicule: that.steps[1].value,
            garanties: that.steps[2].value,
            taxes: res,
            total: res['primeTotaleTTC']
          };
          this.selectedStep = index;
        },
        err => {
          this.toast.showToast();
        }
      );
    } else if (index === 3) {
      this.garanties = [];
      this.api.getGaranties(this.steps[0].value.typeDevis, this.steps[1].value._dateMEC, this.steps[1].value.glace, this.steps[0].value.cin).subscribe(
        res => {
          let arr = [];
          Array.from(res).map(gar => {
            if (gar['code'] === 'RC') arr.unshift(gar);
            else arr.push(gar);
          });
          this.garanties = arr;
          this.selectedStep = index;
        },
        err => {
          this.toast.showToast();

        }
      );
    } else {
      this.selectedStep = index;
    }
  }
  getCRM() {
    let that = this;
    this.crm = 3;
    let imma = this.steps[1].value.typeImma === 'ww' ? "WW" + this.steps[1].value.immatriculationWw.toUpperCase() : this.steps[1].value.immatriculationDefinitf.code + '-' + this.steps[1].value.immatriculationDefinitf.letter.toUpperCase() + '-' + this.steps[1].value.immatriculationDefinitf.ville;
    let fullName = this.steps[0].value.nom + " " + this.steps[0].value.prenom;
    this.api.getCrm(imma, fullName).subscribe(
      res => {
        that.crm = res;
      },
      err => {
        that.crm = 3;
        console.log("ERROR :" + err.message);
      }
    );
  }
  navigate(index: number) {
    if (index < this.selectedStep) {
      this.goto(index);
    } else {
      return;
    }
  }

  changePaymentType(event) {
    this.payment = event
  }
}
