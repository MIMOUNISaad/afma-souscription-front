import { Toaster } from './../../providers/toast/toast';

import { ApiProvider } from './../../providers/api.provider';
import { Component, OnInit, ViewChild } from '@angular/core';

import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { Router } from '@angular/router';
import { EditPasswordModalComponent } from 'src/app/components';

export interface devisDtata {
  
  CIN: string;
  Nom: string;
  immatriculation: string;
  debutcouverture: string;
  fincouverture: string;
  action: string;
}
@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css']
})
export class ManagementComponent implements OnInit {
  displayedColumns: string[] = ['CIN', 'Nom', 'immatriculation', 'debutcouverture', 'fincouverture', 'action'];
  dataSource: MatTableDataSource<devisDtata>;

  //@ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('productPaginator') productPaginator: MatPaginator;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  data: any;

  pagination: any = {
    page: 0
  };

  constructor(
    private readonly api: ApiProvider,
    private auth: AuthService,
    private router: Router,
    private dialog: MatDialog,
    private toast: Toaster
    ) {
      let myuserconnected = localStorage.getItem('Iduserconnect');
      let roleConnected = localStorage.getItem('userRole');
      if(myuserconnected === null || roleConnected !== 'user') { 
        this.router.navigate(['/manager']);
      }else
       {
         this.showAllDevis(0, 5); 
       }
    }

  ngAfterViewInit() {
    // this.dataSource.paginator = this.productPaginator;
    // this.dataSource.sort = this.sort;
  }

  ngOnInit() {

    // this.api.myuserconnected = localStorage.getItem('Iduserconnect');
    // let myuserconnected = localStorage.getItem('Iduserconnect');

    // if(myuserconnected === null){
    //   this.router.navigate(['/manager']);
    // }
    // else
    // {
    //   this.showAllDevis(0, 5);
    // }

    this.paginator._intl.itemsPerPageLabel = 'Éléments par page:';
    let myuserconnected = localStorage.getItem('Iduserconnect');
    let roleConnected = localStorage.getItem('userRole');
    if(myuserconnected === null || roleConnected !== 'user') {
        this.router.navigate(['/manager']);
      } else
       {
         this.showAllDevis(0, 5);
       }
  }

  showAllDevis(page, pageSize){
    this.api.showAllDevis(page, pageSize).subscribe(
      res => {
        this.data = res['content'];
        this.dataSource = new MatTableDataSource(this.data);
        this.pagination.collectionSize = res['totalElements'];
        this.pagination.pageSize = res['size'];
        this.dataSource.sort = this.sort;
      },
      err => {
        this.pagination.page = 1;
        this.pagination.collectionSize = 0;
        // errors msg
      }
    );
    // this.dataSource.paginator = this.productPaginator;
    //this.dataSource.sort = this.sort;
  }

  getNext(event){
    this.pagination.page = event.pageIndex;

    this.data = [];
    this.showAllDevis(this.pagination.page, 5);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
 validerDevis(idDevis){
    this.api.ValiderDevis(idDevis).subscribe(
      res => {
        this.showAllDevis(this.pagination.page, 5);
        this.data = res;
        this.toast.showSuccessMessage("Devis validé")
      },
      err =>{

        // errors msg
      }
    );
  }

logout() {
    this.auth.logout();
  }
}
