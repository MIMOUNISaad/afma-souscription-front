import { InjectionToken } from "@angular/core";

export let APP_CONFIG = new InjectionToken("app.config");

export const AppConfig: any = {
  app_name: 'AFMA_AMC',
  server_url: 'https://afma-online.ma/sousrciption-back/',
};
