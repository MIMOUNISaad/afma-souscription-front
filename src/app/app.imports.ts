import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ng6-toastr-notifications';
import { HttpClientModule } from '@angular/common/http';
import {MatDialogModule} from '@angular/material/dialog';
//import { RecaptchaModule } from 'angular-google-recaptcha';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask'
import { ROUTES } from './app.routing';
// tslint:disable-next-line: max-line-length
import { MatFormFieldModule , MatTableModule , MatPaginatorModule , MatInputModule, MatButtonModule, MatRippleModule } from '@angular/material';
import { AppMaterialModule } from './app.material.module';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

export const IMPORTS: any[] = [
    RouterModule.forRoot(ROUTES),
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatStepperModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    MatDialogModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatButtonModule,
    MatRippleModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    // RecaptchaModule.forRoot({
    //   siteKey: '6Lc_jdcUAAAAAC_dZy0D3xw1R63PGs4NSOj_1SZH',
    // }),
    NgxMaskModule.forRoot(),
    MatRadioModule,
    FormsModule,
    AppMaterialModule,
    MatButtonToggleModule
];
