import { Routes } from '@angular/router';
import * as PAGES from './pages/index';
import * as COMPONENTS from './components/index';
import { AuthGuard } from './providers/auth/auth.guard';



export const ROUTES: Routes = [
  {
    path: 'index',
    component: PAGES.MainPageComponent
  },
  {
    path: 'manager',
    component: PAGES.LoginPageComponent
  },
  {
    path: 'demander-devis',

    component: PAGES.DemanderDevisPageComponent
  },
  {
    path: 'management',
    canActivate: [AuthGuard],
    component: PAGES.ManagementComponent,
  },
  {
    path: 'admin-management',
    canActivate: [AuthGuard],
    component: PAGES.AdminManagementComponent,
    children:[

      {
        path:'users',
        component:COMPONENTS.GererUsersComponent
      },
      {
        path:'convention',
        component:COMPONENTS.GererConventionComponent
      },
      {
        path:'convention-param',
        component:COMPONENTS.ConventionParamComponent
      }
    ]
  },
  {
    path: '',
    redirectTo: '/index',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/index',
    pathMatch: 'full'
  }
];

