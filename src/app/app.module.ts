import { AddConventionModalComponent } from './components/add-convention-modal/add-convention-modal.component';
import { AddUserModalComponent } from './components/add-user-modal/add-user-modal.component';
import { ConditionsSpecialComponent } from './components/conditions-special/conditions-special.component';
import { NgModule } from '@angular/core';

import { AppComponent } from './root/app.component';
import { DECLARATIONS } from './app.declarations';
import { IMPORTS } from './app.imports';
import { PROVIDERS } from './app.providers';
import {
  CongratComponent,
  ConventionComponent,
  EditUserModalComponent,
  EditConventionModalComponent,
  AddConvParamModalComponent,
  EditConvParamModalComponent,
  EditPasswordModalComponent,
  DeleteConfirmComponent,
  TierceComponent, CollisionComponent, PersonneTransporteComponent
} from './components';



@NgModule({
  declarations: DECLARATIONS,
  imports: IMPORTS,
  providers: PROVIDERS,
  bootstrap: [AppComponent],
  entryComponents: [
    ConditionsSpecialComponent,
    CongratComponent,
    ConventionComponent,
    AddUserModalComponent,
    EditUserModalComponent,
    AddConventionModalComponent,
    EditConventionModalComponent,
    AddConvParamModalComponent,
    EditConvParamModalComponent,
    EditPasswordModalComponent,
    DeleteConfirmComponent,
    TierceComponent,
    CollisionComponent,
    PersonneTransporteComponent
  ],
})
export class AppModule { }
